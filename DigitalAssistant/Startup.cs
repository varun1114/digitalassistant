﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DigitalAssistant.Startup))]
namespace DigitalAssistant
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
